import { QueryEditorPage } from './app.po';

describe('query-editor App', () => {
  let page: QueryEditorPage;

  beforeEach(() => {
    page = new QueryEditorPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
