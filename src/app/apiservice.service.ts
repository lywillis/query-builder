import { Injectable } from '@angular/core';
import {Http, Response, Headers} from '@angular/http'; 

import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise'

export class API_Table {
  name: string; 
  kind: string; 
  url: string
}

@Injectable()
export class APIService {
  private userName = 'read_only';
  private authToken = '2r2A6r7f9L8k3u9J8h0l';
  private baseUrl = 'http://localhost:8153/api.rsc';
  private headers = new Headers(); 


  constructor(private http: Http) {
    this.headers.append('Authorization', 'Basic ' + btoa(this.userName+":"+this.authToken));
  }

  getTables(): Promise<API_Table[]> {
    return this.http.get(this.baseUrl, {headers: this.headers})
      .toPromise() //response type
      .then(response => response.json().value ); 
      // .catch(this.handleError);
  }

  getColumns(tableName: string): Promise<string[]> {
    return this.http.get(`${this.baseUrl}/${tableName}/$metadata?@json`, {headers: this.headers})
      .toPromise()
      .then(response => response = response.json().items[0]["odata:cname"] )
      //.catch(this.handleError);
  }

  getTableData(tableName:string, columnList: string): Promise<Object[]> {
    return this.http.get(`${this.baseUrl}/${tableName}/?$select=${columnList}`, {headers: this.headers})
      .toPromise()
      .then(response => response = response.json().value )
      //.catch(this.handleError);
  }

}
