import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import {APIService, API_Table} from './apiservice.service'; 
import {TableComponent} from './table/table.component'; 
import {Table} from './table.model'

import 'rxjs/Rx';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  tables: Table[] = []; // tables to be displayed on side 
  
  constructor(private apiService: APIService){
    //this.selectedTable = this.apiService.getTables(); 
  }
  ngOnInit() {
     this.FillTables(); 

    }

    FillTables() {
      this.apiService.getTables()// response
      .then(response => { //if the response is good
        for (let table of response){
          //this.tables.push(new Table(table.name)); 
          this.apiService.getColumns(table.name)
          .then(columns => {
            var columnList = columns.join(',');
            this.apiService.getTableData(table.name, columnList)
            .then(data => this.tables.push(new Table
              (
              table.name, 
              columns, 
              data)))
        
        }) // fills table names and cols
      }}); 

      
    }
  }

