export class Table {
    name: string; 
    columns : string[]; 
    selectedColumns: string[]; 
    data: Object[]; 

    constructor(name: string = "", columns: string[] = [], data = []){
        this.name = name; 
        this.columns = columns; 
        this.data = data; 
    }
}