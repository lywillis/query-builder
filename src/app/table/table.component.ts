import { Component, OnInit, Input} from '@angular/core';

import {Table} from '../table.model'; 


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent {

  @Input() table: Table; 
  constructor() { }

 

}
