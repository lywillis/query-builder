import {Table} from './table.model'; 
import {Operator, Condition, ConditionType} from './utilities'; 


export class Query {
    operators: Operator[]; 
    table: Table; 
    selectedColumns: string[]; 
    conditions: Condition[]; 

    constructor() {
        this.table = new Table();
        this.operators = [];  
        this.conditions = []; 
         
    }

    addTable(newTable: Table){
        this.table = newTable; 
    }
    addCondition(condition: ConditionType, column){
        this.conditions.push(new Condition(column, condition));
    }
    
    addOperator(operator: Operator) {
        this.operators.push(operator); //numeric form
        console.log(this.operators); 
    }

    
}