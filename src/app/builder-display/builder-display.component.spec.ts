import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuilderDisplayComponent } from './builder-display.component';

describe('BuilderDisplayComponent', () => {
  let component: BuilderDisplayComponent;
  let fixture: ComponentFixture<BuilderDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuilderDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuilderDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
