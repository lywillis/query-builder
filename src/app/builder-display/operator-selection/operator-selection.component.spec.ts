import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperatorSelectionComponent } from './operator-selection.component';

describe('OperatorSelectionComponent', () => {
  let component: OperatorSelectionComponent;
  let fixture: ComponentFixture<OperatorSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperatorSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperatorSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
