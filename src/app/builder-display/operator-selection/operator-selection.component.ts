import { Component, OnInit, Input} from '@angular/core';
import {Query} from '../../query.model'

@Component({
  selector: 'operator-selection',
  templateUrl: './operator-selection.component.html',
  styleUrls: ['./operator-selection.component.css']
})
export class OperatorSelectionComponent implements OnInit {
  @Input() operator: string; 
  constructor() { }

  ngOnInit() {
  }

}
