import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Table} from '../table.model'; 
import {Query} from '../query.model'; 
import {Operator, Condition, ConditionType} from '../utilities'; 

@Component({
  selector: 'builder-display',
  templateUrl: './builder-display.component.html',
  styleUrls: ['./builder-display.component.css']
})


export class BuilderDisplayComponent implements OnInit {
  @Input() tables: Table[]; 
  @Output() selectedTable: EventEmitter<Table> /** outputs the current table that is selected */
  private currentTable: Table = new Table(); 
  private currentCol: string = ""; // column currently selected for condition
  query: Query; 
  
  availableOperators: string[]; 
  availableConditions: string[]; 
  
  /** tableSelected: boolean; */
  // selectedColumns: string[]; 
  constructor() {  
  this.selectedTable = new EventEmitter();
  let options = Object.keys(Operator); 
  this.availableOperators = options.slice(options.length/2); //string keys are in second half 
  options = Object.keys(ConditionType)
  this.availableConditions = options.slice(options.length/2); 
  console.log(this.availableConditions); 
  this.currentCol = ""; 
}
  

  ngOnInit() {
    this.query = new Query(); 
    console.log(this.availableOperators); 
  }
  tableClicked(table: Table) : void {
    this.currentTable = table; 
    this.selectedTable.emit(table); 
    this.query.addTable(table); 
    console.log(this.currentTable.name); 
  }

  operatorClicked(selection: string) : void {
    var operator: Operator = Operator[selection]; //enum value of the button click
    this.query.addOperator(operator); 
  }

  conditionClicked(selection: string) : void {
    var condition: ConditionType = ConditionType[selection]; 
    if(this.currentCol !== ""){
      this.query.addCondition(condition, this.currentCol);
      this.currentCol = ""; // reset col
    } 
  }
    

  }


