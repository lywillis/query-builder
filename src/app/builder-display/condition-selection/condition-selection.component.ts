import { Component, OnInit, Input} from '@angular/core';

import {Table} from '../../table.model'; 

@Component({
  selector: 'condition-selection',
  templateUrl: './condition-selection.component.html',
  styleUrls: ['./condition-selection.component.css']
})
export class ConditionSelectionComponent implements OnInit {
  @Input() condition: string; 
  @Input() currentTable: Table; 
  constructor() { }
  ngOnInit() {
  }

}
