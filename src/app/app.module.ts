import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

//import { AngularFireModule } from 'angularfire2';

// New imports to update based on AngularFire2 version 4
//import { AngularFireDatabaseModule } from 'angularfire2/database';
//import { AngularFireAuthModule } from 'angularfire2/auth';
import {APIService} from './apiservice.service';


import {Table} from './table.model';
import { TableComponent } from './table/table.component';

import { QueryWindowComponent } from './query-window/query-window.component';
import { BuilderDisplayComponent } from './builder-display/builder-display.component';
import { TableSelectionComponent } from './builder-display/table-selection/table-selection.component';
import { OperatorSelectionComponent } from './builder-display/operator-selection/operator-selection.component';
import { ConditionSelectionComponent } from './builder-display/condition-selection/condition-selection.component'; 

export const firebaseConfig = {
  apiKey: "AIzaSyBDRttkKY5ubBvuvHfxUzPZ1Mh6OHRr3kU",
  authDomain: "castproject-975b3.firebaseapp.com",
  databaseURL: "https://castproject-975b3.firebaseio.com",
  projectId: "castproject-975b3",
  storageBucket: "castproject-975b3.appspot.com",
  messagingSenderId: "596028018756"
};

@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
 
    QueryWindowComponent,
 
    BuilderDisplayComponent,
 
    TableSelectionComponent,
 
    OperatorSelectionComponent,
 
    ConditionSelectionComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    
  ],
  providers: [APIService],
  bootstrap: [AppComponent]
})
export class AppModule { }
