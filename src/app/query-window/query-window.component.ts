import { Component, OnInit, Input } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms'; 
import {Table} from '../table.model'; 
import {Query} from '../query.model'; 
import {Operator, Condition, ConditionType} from '../utilities'; 

@Component({
  selector: 'query-window',
  templateUrl: './query-window.component.html',
  styleUrls: ['./query-window.component.css']
})
export class QueryWindowComponent implements OnInit{
  @Input() selectedTable: Table; 
  @Input() query: Query; 
  Operator = Operator; 
  ConditionType = ConditionType; 
  constructor() { 
   }
   ngOnInit() {
     
   }




}
