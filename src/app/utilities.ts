// export  class Utilities{
//     static enumToArray(enumObject: Object) : string[] {
//     let result= []; 
//     for(var val in enumObject) {
//         if(typeof(enumObject[val] === "string")){
//                 result.push(val); 
//             } 
//         }
//         return result; 
//     }
// }
export enum Operator {
    AND,
    OR,
    NOT
}

export enum ConditionType {
    "=",
    "<",
    ">"

}

export class Condition {
    type: ConditionType; 
    column: string; 
    constructor(column: string, type: ConditionType){
        this.column = column; 
        this.type = type; 
    }
}


// export class Operator {
//     static type: OperatorType; 
//     constructor(type: OperatorType) {
//         this.type = type; 
// }